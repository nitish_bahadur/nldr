#!/bin/env python3
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -p short
#SBATCH -t 24:00:00

from keras.layers import Input, Dense, Lambda
from keras.models import Model
from keras import losses
from keras import optimizers
from keras import regularizers
import tensorflow as tf
import numpy as np
import os

from datetime import datetime
import time
import sys
import logging

import json
from keras.callbacks import Callback
from keras import backend as K
from keras.regularizers import Regularizer


NUM_EPOCHS = 30000
BATCH_SIZE = 10
LR = 1e-5
INPUT_DIR = r'../data/spy/daily/input/returns'
THR = 2e-6

def get_file_path(root_dir, date_str, img_type=None):
    if img_type is None:
        file_path = os.path.join(root_dir, date_str + '.dat')
    else:
        file_path = os.path.join(root_dir, date_str + img_type + '.dat')
    return file_path

def load_data(date_str):
    file_path = get_file_path(INPUT_DIR, date_str)
    m = np.load(file_path)  
    return m

def save_history(history, filename):
    with open(filename, 'w') as f:
        json.dump(history.history, f)

def get_output_base_dir(L1):
    base_dir = r'../data/spy/daily/results/returns/regularizer_plus_lambda/norm-lambda-' + str(L1)
    return base_dir

def get_output_dir(L1):
    output_dir = get_output_base_dir(L1) + '/output'
    return output_dir

# ----------------------------------------------------------------------
# Custom Activity Regularizer
# Here we are regularizing the l2_normalize value
class L2L1ActivityRegularizer(Regularizer):
    def __init__(self, l1=0.01):
        self.l1 = l1

    def __call__(self, x):
        regularization = 0            
        x = K.l2_normalize(x, axis=1)
        regularization += K.sum(self.l1 * K.abs(x))
        return regularization

    def get_config(self):
        return {"name": self.__class__.__name__}     

# ------------------------------------------------------------------------
# A custom class that implements exiting early stopping based on loss
# value.  We want to run this by default till 1e-6
# ------------------------------------------------------------------------
class EarlyStoppingByLossVal(Callback):
    def __init__(self, logger, monitor='val_loss', value=1e-5, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose
        self.logger = logger
        self.num_epochs = NUM_EPOCHS

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            logger.debug("Early stopping requires %s available!" % self.monitor, RuntimeWarning)

        if current < self.value:
            if self.verbose > 0:
                logger.debug("Epoch %05d: early stopping THR" % epoch)
                self.num_epochs = epoch
            self.model.stop_training = True

    def get_num_epochs(self):
        return self.num_epochs

# -----------------------------------------------------------------------
# average the z values across the day
# see if the z_i >= threshold, in this 
# case 1%, we expect mean z is sent in
# -----------------------------------------------------------------------
def count_gt_threshold(z, threshold):
    tot = sum(z)
    z_pct = [(i/tot) for i in sorted(z, reverse=True)]
    z_gt_theta = [i for i in z_pct if i >= threshold]
    return len(z_gt_theta)

# -----------------------------------------------------------------------
# start adding the largest values first; keep adding 
# till you get 90%
# -----------------------------------------------------------------------
def count_upto_threshold(z, threshold):
    tot = sum(z)
    z_pct = [(i/tot) for i in sorted(z, reverse=True)]
    cum_z_pct = np.cumsum(z_pct)
    for i in range(0, cum_z_pct.shape[0]):
        if cum_z_pct[i] >= 0.9:
            return i+1
    return cum_z_pct.shape[0]

""" 
    This the simplest auto encoder.  
"""
def build_model(input_dim, L1, logger):
    # encoder
    input_img = Input(shape=(input_dim,))
    encoded = Dense(256, activation='relu', name='Layer1_Encoder')(input_img)
    layer2_encoded = Dense(128, activation=None, name='Layer2_Encoder')(encoded)

    # Regularizer uses the l2 normalized input and we take the l1 normalization
    l2l1_regularizer = L2L1ActivityRegularizer(L1)
    z_layer_encoded = Dense(64, activation=None, activity_regularizer=l2l1_regularizer, name='Z_Layer')(layer2_encoded)

    # l2 normalized output
    z_layer_output_l2_norm_encoded = Lambda(lambda  x: K.l2_normalize(x,axis=1), name='L2_normalize')(z_layer_encoded)

    # decoder
    decoded = Dense(128, activation=None, name='Layer2_Decoder')(z_layer_encoded)
    decoded = Dense(256, activation='relu', name='Layer1_Decoder')(decoded)
    decoded = Dense(input_dim, activation='tanh', name='Output_Layer')(decoded)

    layer2_encoder = Model(input_img, z_layer_output_l2_norm_encoded)
    encoder = Model(input_img, z_layer_encoded)
    autoencoder = Model(input_img, decoded)
    
    return encoder, autoencoder, layer2_encoder


"""
 Set up log files
"""
def setup_log_file(date_to_process, L1, logger):
    output_dir = get_output_base_dir(L1) + r'/logs/'
    try:
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
    except FileExistsError:
        pass

    fh = logging.FileHandler(get_output_base_dir(L1) + r'/logs/' + date_to_process + '.log')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return fh      

"""
Load training data based on date
"""
def get_training_data(date_to_process, logger):
    logger.debug('Date to process: {}'.format(date_to_process))
    x_train = load_data(date_to_process)
    x_train = x_train[:,:-2]
    x_train = x_train.astype('float32') / (np.max(x_train) - np.min(x_train))
    x_test = x_train
    return x_train, x_test

"""
Log config parameters for debugging
"""
def log_config(L1, logger):
    logger.debug('INPUT_DIR: {0:s}'.format(INPUT_DIR)) 
    logger.debug('OUTPUT_DIR: {0:s}'.format(get_output_dir(L1))) 
    logger.debug('L1 Regularizer: {0:s}'.format(str(L1))) 
    logger.debug('Learning Rate: {0:s}'.format(str(LR))) 
    logger.debug('NUM_EPOCHS: {}'.format(str(NUM_EPOCHS))) 
    logger.debug('EarlyStoppping THR: {}'.format(str(THR))) 

"""
Run the input through the tensor to see the output.
We will use the output to determine the dimensionality of data
"""
def get_predict_imgs(x_test, history, layer2_encoder, encoder, autoencoder, logger):
    layer2_encoded_imgs = layer2_encoder.predict(x_test)
    encoded_imgs = encoder.predict(x_test)
    decoded_imgs = autoencoder.predict(x_test)

    logger.debug('x_test min: {0:f} max: {1:f}'.format(np.min(x_test[-1]), np.max(x_test[-1])))
    print('l2_norm_encoded_imgs min: {0:f} max: {1:f}'.format(np.min(layer2_encoded_imgs[-1]), np.max(layer2_encoded_imgs[-1])))
    logger.debug('encoded_imgs min: {0:f} max: {1:f}'.format(np.min(encoded_imgs[-1]), np.max(encoded_imgs[-1])))
    logger.debug('decoded_imgs min: {0:f} max: {1:f}'.format(np.min(decoded_imgs[-1]), np.max(decoded_imgs[-1])))
    logger.debug('final loss: {:.8f}'.format(history.history['loss'][-1]))

    return layer2_encoded_imgs, encoded_imgs, decoded_imgs

# sort the rows on the matrix and then calculate mean
def sort_by_row(z):
    z_sorted = None
    for i in np.arange(z.shape[0]):
        z_s = sorted(z[i,:], reverse=True)
        if z_sorted is None:
            z_sorted = z_s
        else:
            z_sorted = np.vstack((z_sorted,z_s))
    return z_sorted

# calculate mean of sorted rows
def row_sort_column_mean(m):
    z = abs(m)
    z = sort_by_row(z)
    z = np.mean(z, axis=0) # col wise mean
    z = sorted(z, reverse=True)
    return z

# ----------------------------------------------------------
# Calculate the dimension based on z value.
# ----------------------------------------------------------
def calculate_dimension(layer2_encoded_imgs, encoded_imgs, logger):
    z = row_sort_column_mean(encoded_imgs)  
    logger.debug('Z Layer dimension abs(z) each GTE 1%: {} '.format(count_gt_threshold(z, 0.01)))
    logger.debug('Z Layer dimension abs(z) upto 90%: {} '.format(count_upto_threshold(z, 0.9)))

    # based on l2_normalized output of z layer
    z = row_sort_column_mean(layer2_encoded_imgs)
    logger.debug('L2 Normalized Z Layer dimension abs(z) each GTE 1%: {} '.format(count_gt_threshold(z, 0.01)))
    logger.debug('L2 Normalized Z Layer dimension abs(z) upto 90%: {} '.format(count_upto_threshold(z, 0.9)))

# -----------------------------------------------------------
# Get weights and biases of layer before Z_layer
# and Z_layer.  This will help us understand the interplay 
# of l2_normalization.
# -----------------------------------------------------------
def get_weights_biases(model, layer_name):
    the_layer = model.get_layer(layer_name)
    return the_layer.get_weights()[0], the_layer.get_weights()[1]


def save_weights_biases(layer_weights, layer_biases, L1, date_to_process, layer_prefix):
    layer_weights.dump(get_file_path(get_output_dir(L1), date_to_process, '_{}_weights'.format(layer_prefix)))
    layer_biases.dump(get_file_path(get_output_dir(L1), date_to_process, '_{}_biases'.format(layer_prefix)))



# ----------------------------------------------------------
# Calculate the dimension based on z value.
# ----------------------------------------------------------
def save_output_files(date_to_process, L1, history, layer2_encoded_imgs, encoded_imgs, decoded_imgs, model):
    output_dir = get_output_dir(L1)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    layer2_encoded_imgs.dump(get_file_path(get_output_dir(L1), date_to_process, '_z_layer_l2_norm'))
    encoded_imgs.dump(get_file_path(get_output_dir(L1), date_to_process, '_encoded'))

    filename = os.path.join(get_output_dir(L1), date_to_process + '_history'  + '.json')
    save_history(history, filename)

    # --------------------------- SAVE ENCODER WEIGHTS AND BIASES --------------------------------------
    # layer1_weights, layer1_biases = get_weights_biases(model, "Layer1_Encoder")
    # save_weights_biases(layer1_weights, layer1_biases, L1, date_to_process, 'layer1_encoder')

    # layer2_weights, layer2_biases = get_weights_biases(model, "Layer2_Encoder")
    # save_weights_biases(layer2_weights, layer2_biases, L1, date_to_process, 'layer2_encoder')

    # --------------------------- SAVE HIDDEN LAYER WEIGHTS AND BIASES --------------------------------------
    z_layer_weights, z_layer_biases = get_weights_biases(model, "Z_Layer")
    save_weights_biases(z_layer_weights, z_layer_biases, L1, date_to_process, 'z_layer')

    # --------------------------- DECODER WEIGHTS AND BIASES --------------------------------------
    # layer2_weights, layer2_biases = get_weights_biases(model, "Layer2_Decoder")
    # save_weights_biases(layer2_weights, layer2_biases, L1, date_to_process, 'layer2_decoder')

    # layer1_weights, layer1_biases = get_weights_biases(model, "Layer1_Decoder")
    # save_weights_biases(layer1_weights, layer1_biases, L1, date_to_process, 'layer1_decoder')


# ------------------------------------------------------------------------
# The function the goes through the steps of determining the 
# dimensionality of the data set.
# ------------------------------------------------------------------------
def get_dimension(date_to_process, x_train, L1, logger):
    logger.debug('Input file to process: {0:s}'.format(date_to_process))  
    logger.debug('Shape of input file: ({0:d}, {1:d})'.format(x_train.shape[0], x_train.shape[1]))

    # use even number of features so that we can plot
    # things in a matrix if required
    input_dim = x_train.shape[1]

    encoder, autoencoder, layer2_encoder = build_model(input_dim, L1, logger)

    autoencoder.compile(loss=losses.mean_squared_error, 
    	optimizer=optimizers.Adam(lr=LR, epsilon=1e-08))

    callbacks = [
        EarlyStoppingByLossVal(logger, monitor='loss', value=THR, verbose=1)
    ]

    history = autoencoder.fit(x_train, x_train, 
    	epochs=NUM_EPOCHS, 
    	batch_size=BATCH_SIZE, 
    	shuffle=False, 
        verbose=2, 
    	callbacks=callbacks)

    layer2_encoded_imgs, encoded_imgs, decoded_imgs = get_predict_imgs(x_train, 
        history,
        layer2_encoder, 
        encoder, 
        autoencoder, 
        logger)
    
    calculate_dimension(layer2_encoded_imgs, encoded_imgs, logger)
    
    # save the output files
    save_output_files(date_to_process, L1, history, layer2_encoded_imgs, encoded_imgs, decoded_imgs, autoencoder)

    training_loss = history.history['loss'][-1]
    early_stopping = callbacks[0]
    num_epochs = early_stopping.get_num_epochs()
    return training_loss, num_epochs

# the main routine that drives all the processing
def main_driver(date_to_process, L1, logger):
    total_time = time.time()

    fh = setup_log_file(date_to_process, L1, logger)
    x_train, x_test = get_training_data(date_to_process, logger)
    log_config(L1, logger)

    if x_train is None:
        logger.debug('Input file for {0:s} not present'.format(date_to_process))  
    else:        
        start_time = time.time()
        training_loss, epoch = get_dimension(date_to_process, x_train, L1, logger)
        logger.debug('[%d],%.7f,%d,%d seconds'% (64, training_loss, epoch, int(time.time() - start_time)))


    logger.debug('{0:s} process completed! in {1:d} seconds.'.format(date_to_process, int(time.time() - total_time)))

    logger.removeHandler(fh)

# ---------------------------------------------------------------
# Setup console logger
# ----------------------------------------------------------------
def setup_console_logger():
    logger = logging.getLogger('simple_logger')
    logger.setLevel(logging.DEBUG)

    con = logging.StreamHandler()
    con.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    con.setFormatter(formatter)
    logger.addHandler(con)
    return logger

# the program main
if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    logger = setup_console_logger()

    # run for which date
    dates = [sys.argv[1]]

    # run for which lambda
    # L1 = float(sys.argv[2])
    L1 = float('6e-8')

    for date_to_process in dates:
        main_driver(date_to_process, L1, logger)