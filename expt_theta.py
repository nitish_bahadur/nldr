import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import logging
import os.path

from sklearn import manifold
from numpy.linalg import svd
from scipy.spatial.distance import pdist
from numpy.linalg import norm

import calc_dim
import utils

def get_returns_df(filename):
    """
	Load the data and calculate log return.  Remove the 
    first row because it will always be NaN.

    Parameters
    ----------
    filename : str
			   Name of the file containing data
			   
    Returns
    -------
    df : pandas.DataFrame
        DataFrame containing return time series

	"""
    df = pd.read_csv(filename)
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    df_ret.drop(['1/2/1986'], inplace=True)
    return df_ret
	
def calc_dim_ts(df, width, knn, logger):
    """Loop through using PCA, MDS and IsoMap"""
    max_rows = df.shape[0] - (width + 1)    
    dates = []
    pca_svd_dimensions = []
    mds_svd_dimensions = []
    isomap_svd_dimensions = []

    pca_eig_dimensions = []
    mds_eig_dimensions = []
    isomap_eig_dimensions = []
    
    theta = 0.01
    
    for i in range(0, max_rows):
        data = df[i:i+width].copy()
        orig_cols_len = len(data.columns)
        data.dropna(axis=1, inplace=True)
        new_cols_len = len(data.columns)
        ds = df.index[i+width]
        
        s = '%d.)  orig_cols_len=%d   new_cols_len=%d    date=%s\n' % (i, orig_cols_len, new_cols_len, ds)
        logger.write(s)
        
        # PCA/SVD
        U, S, V = np.linalg.svd(data)
        svd_dim = calc_dim.get_svd_dim_gt_theta(S, theta)
        eig_dim = calc_dim.get_eig_dim_gt_theta(S, theta)
        pca_svd_dimensions.append(svd_dim)
        pca_eig_dimensions.append(eig_dim)
        s = 'SVD PCA = %d   EIG PCA = %d\n' % (svd_dim, eig_dim)
        logger.write(s)
        
        # MDS
        Y = manifold.MDS(n_components=width, max_iter=300, n_init=4).fit_transform(data)
        Y = Y[:,~np.all(np.isnan(Y), axis=0)]
        U, S, V = np.linalg.svd(Y)
        svd_dim = calc_dim.get_svd_dim_gt_theta(S, theta)
        eig_dim = calc_dim.get_eig_dim_gt_theta(S, theta)
        mds_svd_dimensions.append(svd_dim)
        mds_eig_dimensions.append(eig_dim)
        s = 'SVD MDS = %d   EIG MDS = %d\n' % (svd_dim, eig_dim)
        logger.write(s)

        
        # IsoMap
        Y = manifold.Isomap(n_neighbors=knn, n_components=width).fit_transform(data)
        Y = Y[:,~np.all(np.isnan(Y), axis=0)]
        U, S, V = np.linalg.svd(Y)
        svd_dim = calc_dim.get_svd_dim_gt_theta(S, theta)
        eig_dim = calc_dim.get_eig_dim_gt_theta(S, theta)
        isomap_svd_dimensions.append(svd_dim)
        isomap_eig_dimensions.append(eig_dim)
        s = 'SVD ISO = %d   EIG ISO = %d\n' % (svd_dim, eig_dim)
        logger.write(s)
        
        dates.append(ds)
        logger.write('------------------------------------------------------------------------\n')
                        
    df1 = pd.DataFrame()
    df1['Dates'] = dates
    df1['PCA_SVD_DIMS'] = pca_svd_dimensions
    df1['MDS_SVD_DIMS'] = mds_svd_dimensions
    df1['ISO_SVD_DIMS'] = isomap_svd_dimensions

    df1['PCA_EIG_DIMS'] = pca_eig_dimensions
    df1['MDS_EIG_DIMS'] = mds_eig_dimensions
    df1['ISO_EIG_DIMS'] = isomap_eig_dimensions
    
    return df1

def run_expt():
	save_path = 'C:\\workspace\\nldr\\log'
	name_of_file = 'russell3000.log'
	log_file_name = os.path.join(save_path, name_of_file)
	logger = open(log_file_name, 'w')

	logger.write('Process started\n')
	logger.write('Loading russell3000.csv\n')
	df = get_returns_df('russell3000.csv')
	logger.write('File loaded successfully\n')
	
	
	widths = [15, 20, 30, 40, 50, 60]
	for width in widths:
		df_dim_ts = calc_dim_ts(df, width, 10, logger)
		file_name = utils.get_csv_filename('expt_theta', width)
		df_dim_ts.to_csv(file_name)
		logger.write('Dimensionality time series for width %d generated.  Now plot.\n' % width)    
	
	logger.close()