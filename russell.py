import numpy as np
import pandas as pd
import os.path
from sklearn import manifold
from numpy.linalg import svd


def get_returns_daily_df(filename):
    """Load the data and calculate log return.  Remove the 
    first row because it will always be NaN."""
    df = pd.read_csv(filename) # 'russell3000.csv'
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    df_ret.drop(['1/2/1986'], inplace=True)
    return df_ret

def get_returns_df(filename):
	"""Load the data and calculate log return.  
	Remove the first row because it will always be NaN."""
	df = pd.read_csv(filename)
	df.set_index(df['DateTime'], inplace=True)
	df.drop('DateTime', axis=1, inplace=True)
	df_ret = np.log(df / df.shift())
	df_ret.drop(df.head(1).index, inplace=True)
	
	ts = pd.to_datetime(df_ret.index, format='%Y%m%d %H:%M:%S')	
	mask = (ts.strftime('%H:%M:%S') == "09:30:00") | (ts.strftime('%H:%M:%S') == "09:31:00")
	
	df_ = df_ret[~mask]
	
	return df_
	
def get_svd_dimension(S, threshold=0.9):
	tot = sum(S)
	s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
	cum_s_exp = np.cumsum(s_exp)
	for i in range(0, cum_s_exp.shape[0]):
		if cum_s_exp[i] >= threshold:
			return i
	return cum_s_exp.shape[0]    
	
	
def get_eig_dimension(S, threshold=0.9):
	"""Calculate the dimension based on cumulative eigenvalue value percentages"""
	return get_svd_dimension(np.square(S), threshold**2)
	
	
def get_svd_dim_gt_theta(S, theta = 0.01):
	"""Calculate the dimension based on cumulative singular value percentages"""
	tot = sum(S)
	s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
	s_gt_theta = [i for i in s_exp if i >= theta]
	return len(s_gt_theta)

def get_eig_dim_gt_theta(S, theta = 0.01):
	return get_svd_dim_gt_theta(np.square(S), theta**2)
	

def calc_dim_ts(df, width, logger, threshold = 0.9, knn=10):
	"""Loop through using PCA, MDS and IsoMap"""
	max_rows = df.shape[0] - (width + 1)    
	dates = []
	pca_svd_dimensions = []
	mds_svd_dimensions = []
	isomap_svd_dimensions = []
	
	pca_eig_dimensions = []
	mds_eig_dimensions = []
	isomap_eig_dimensions = []
    
	for i in range(0, max_rows):
		data = df[i:i+width].copy()
		orig_cols_len = len(data.columns)
		data.dropna(axis=1, inplace=True)
		new_cols_len = len(data.columns)
		ds = df.index[i+width]
		
		s = '%d.)  orig_cols_len=%d   new_cols_len=%d    date=%s\n' % (i, orig_cols_len, new_cols_len, ds)
		logger.write(s)
		
		# PCA/SVD
		U, S, V = np.linalg.svd(data)
		svd_dim = get_svd_dimension(S, threshold)
		eig_dim = get_eig_dimension(S, threshold)
		pca_svd_dimensions.append(svd_dim)
		pca_eig_dimensions.append(eig_dim)
		
		s = 'SVD PCA = %d   EIG PCA = %d\n' % (svd_dim, eig_dim)
		logger.write(s)
		
		# MDS
		Y = manifold.MDS(n_components=width, max_iter=300, n_init=4).fit_transform(data)
		Y = Y[:,~np.all(np.isnan(Y), axis=0)]
		U, S, V = np.linalg.svd(Y)
		svd_dim = get_svd_dimension(S, threshold)
		eig_dim = get_eig_dimension(S, threshold)
		mds_svd_dimensions.append(svd_dim)
		mds_eig_dimensions.append(eig_dim)
		s = 'SVD MDS = %d   EIG MDS = %d\n' % (svd_dim, eig_dim)
		
		logger.write(s)
		
		# IsoMap
		Y = manifold.Isomap(n_neighbors=knn, n_components=width).fit_transform(data)
		Y = Y[:,~np.all(np.isnan(Y), axis=0)]
		U, S, V = np.linalg.svd(Y)
		svd_dim = get_svd_dimension(S, threshold)
		eig_dim = get_eig_dimension(S, threshold)
		isomap_svd_dimensions.append(svd_dim)
		isomap_eig_dimensions.append(eig_dim)
		
		s = 'SVD ISO = %d   EIG ISO = %d\n' % (svd_dim, eig_dim)
		logger.write(s)
		
		dates.append(ds)
		
		s = '------------------------------------------------------------------------\n'
		logger.write(s)
	
	df1 = pd.DataFrame()
	df1['Dates'] = dates
	df1['PCA_SVD_DIMS'] = pca_svd_dimensions
	df1['MDS_SVD_DIMS'] = mds_svd_dimensions
	df1['ISO_SVD_DIMS'] = isomap_svd_dimensions
	
	df1['PCA_EIG_DIMS'] = pca_eig_dimensions
	df1['MDS_EIG_DIMS'] = mds_eig_dimensions
	df1['ISO_EIG_DIMS'] = isomap_eig_dimensions
	
	return df1
	
def calc_isomap_dim_ts(df, width):
	"""Loop through for isomap with different knn/width IsoMap"""
	max_rows = df.shape[0] - (width + 1)    
	dates = []
	isomap_svd_dim_k10 = []
	isomap_eig_dim_k10 = []
	isomap_svd_dim_k20 = []
	isomap_eig_dim_k20 = []
	isomap_svd_dim_k30 = []
	isomap_eig_dim_k30 = []
	isomap_svd_dim_k40 = []
	isomap_eig_dim_k40 = []
	isomap_svd_dim_k50 = []
	isomap_eig_dim_k50 = []
    
	threshold = 0.9
	knns = [10, 20, 30, 40, 50]
	
	for i in range(0, max_rows):
		data = df[i:i+width].copy()
		orig_cols_len = len(data.columns)
		data.dropna(axis=1, inplace=True)
		new_cols_len = len(data.columns)
		ds = df.index[i+width]

		s = '%d.)  orig_cols_len=%d   new_cols_len=%d    date=%s\n' % (i, orig_cols_len, new_cols_len, ds)
		logger.write(s)
		
		# IsoMap
		for knn in knns:
			Y = manifold.Isomap(n_neighbors=knn, n_components=width).fit_transform(data)
			Y = Y[:,~np.all(np.isnan(Y), axis=0)]
			U, S, V = np.linalg.svd(Y)
			svd_dim = get_svd_dimension(S, threshold)
			eig_dim = get_eig_dimension(S, threshold)
			if knn == 10:
				isomap_svd_dim_k10.append(svd_dim)
				isomap_eig_dim_k10.append(eig_dim)
			elif knn == 20:
				isomap_svd_dim_k20.append(svd_dim)
				isomap_eig_dim_k20.append(eig_dim)
			elif knn == 30:
				isomap_svd_dim_k30.append(svd_dim)
				isomap_eig_dim_k30.append(eig_dim)
			elif knn == 40:
				isomap_svd_dim_k40.append(svd_dim)
				isomap_eig_dim_k40.append(eig_dim)
			elif knn == 50:
				isomap_svd_dim_k50.append(svd_dim)
				isomap_eig_dim_k50.append(eig_dim)

			s = 'knn = %d    width = %d    SVD ISO = %d   EIG ISO = %d\n' % (knn, width, svd_dim, eig_dim)
			logger.write(s)
        
		dates.append(ds)
		logger.write('------------------------------------------------------------------------\n')
                        
		df1 = pd.DataFrame()
		df1['Dates'] = dates
		df1['ISO_SVD_DIM_K10'] = isomap_svd_dim_k10
		df1['ISO_EIG_DIM_K10'] = isomap_eig_dim_k10

		df1['ISO_SVD_DIM_K20'] = isomap_svd_dim_k20
		df1['ISO_EIG_DIM_K20'] = isomap_eig_dim_k20

		df1['ISO_SVD_DIM_K30'] = isomap_svd_dim_k30
		df1['ISO_EIG_DIM_K30'] = isomap_eig_dim_k30

		df1['ISO_SVD_DIM_K40'] = isomap_svd_dim_k40
		df1['ISO_EIG_DIM_K40'] = isomap_eig_dim_k40

		df1['ISO_SVD_DIM_K50'] = isomap_svd_dim_k50
		df1['ISO_EIG_DIM_K50'] = isomap_eig_dim_k50

		return df1
	
def get_csv_filename(width, output_filename):
	file_name = os.path.join(os.getcwd(), output_filename)
	return file_name

def get_isomap_csv_filename(width):
	save_path = 'C:\\workspace\\nldr\\log'
	name_of_file =  'Intraday2011-isomap_' + str(width) + '.csv'
	file_name = os.path.join(save_path, name_of_file)
	return file_name	

def main(input_filename, output_filename, log_file, ww, threshold, knn):
	log_file_name = os.path.join(os.getcwd(), log_file)
	logger = open(log_file_name, 'w')
	
	logger.write('Process started\n')
	logger.write('Loading intraday russell3000\n')
	df = get_returns_daily_df(input_filename)
	logger.write('File loaded successfully\n')
	
	widths = [ww]
	
	for width in widths:
		df_dim_ts = calc_dim_ts(df, width, logger, threshold, knn)
		file_name = get_csv_filename(width, output_filename)
		df_dim_ts.to_csv(file_name)
		logger.write('Dimensionality time series for width %d generated.  Now plot.\n' % width)    
	
	logger.close()


def imml_main(input_filename, output_filename, log_file, ww, threshold, knn):
	log_file_name = os.path.join(os.getcwd(), log_file)
	logger = open(log_file_name, 'w')
	
	logger.write('Process started\n')
	logger.write('Loading intraday russell3000\n')
	df = get_returns_daily_df(input_filename)
	logger.write('File loaded successfully\n')
	
	# ---------------------------------------------------------------
	# convert the returns to probability distribution by using
	# kernel density estimator
	# ---------------------------------------------------------------
	logger.write('Calculating information distance matrix\n')
	H = get_info_dist_matrix(df)
	logger.write('Information distance matrix calculated successfully\n')
		
	logger.close()

def get_info_dist_matrix(df):
	return df
	
def print_full(x):
	pd.set_option('display.max_rows', len(x))
	print(x)
	pd.reset_option('display.max_rows')	
	
def intraday_main(input_filename, log_file, ww, threshold, knn):
	log_file_name = os.path.join(os.getcwd(), log_file)
	logger = open(log_file_name, 'w')
	
	logger.write('Process started\n')
	logger.write('Loading intraday russell3000\n')
	df = get_returns_df(input_filename)
	logger.write('File loaded successfully\n')
	
	ts = pd.to_datetime(df.index, format='%Y%m%d %H:%M:%S')
	
	minutes = np.unique(ts.strftime('%H:%M:%S'))
	minutes = minutes.tolist()

	file_name = os.path.join(os.getcwd(), 'minutes.csv')
	
	for m in minutes:
		mask = ts.strftime('%H:%M:%S') == m
		df_ = df[mask]
		width = 30
		df1 = calc_dim_ts(df_, width, logger)
		
		#ts = pd.to_datetime(df['Dates'], format='%Y%m%d %H:%M:%S')
		#df1.set_index(ts, inplace=True)
		print_full(df1)
				
		df1.to_csv(file_name, mode='a', header=False)
		
		logger.write('Dimensionality time series for %s generated.\n' % m)    		
	
	logger.close()	