﻿import numpy as np
import os
import sys
import pandas as pd
from dateutil import parser
import scipy.sparse
import math

def get_returns_daily_df(filename):
    """Load the data and calculate log return.  Remove the first row because it will always be NaN."""
    df = pd.read_csv(filename) # 'russell3000_eod_prices.csv'
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    return df_ret.ix[1:]
	
def get_full_path_2_save(root_dir, filename):
    full_path = os.path.join(root_dir, filename)    
    return full_path	

"""
def get_distance_matrix(row):
    _row = row[~np.isnan(row)]
    row_len = len(_row)
    distance_matrix = np.empty((row_len, row_len))
    distance_matrix[:] = np.nan
    for i in range(0, row_len):
        for j in range(0, row_len):
            if (i == j):
                if not math.isnan(_row[j]):
                    distance_matrix[i,j] = 0
                else:
                    distance_matrix[i,j] = math.nan
            else:
                if ((not math.isnan(_row[i])) & (not math.isnan(_row[j]))):
                    distance_matrix[i,j] = abs(_row[i] - _row[j])
                else:
                    distance_matrix[i,j] = math.nan
    tickers = [ind for ind, val in _row.iteritems()]
    return tickers, distance_matrix	
"""

def get_distance_matrix(row):
    _row = row[~np.isnan(row)]
    row_len = len(_row)
    distance_matrix = np.empty((row_len, row_len))
    for i in range(0, row_len):
        D = abs(_row - _row[i])
        distance_matrix[i,:] = D.values.T
    tickers = [ind for ind, val in _row.iteritems()]
    return tickers, distance_matrix	
	
if __name__ == "__main__":
	# Read input file with end of day prices
	INPUT_FILE = r"C:\phd\input\russell3000_eod.csv"
	df = get_returns_daily_df(INPUT_FILE)
	df = df.round(7)

	# --------------------------------------------------------------------
	# Read the S&P 500 constituents
	# This is done to reduce the size of the matrix we have to work with
	# --------------------------------------------------------------------
	#sp500_constituents = pd.read_csv(r"C:\phd\input\sp500_constituents.csv")
	
	#tickers_found = set(sp500_constituents['Symbol']) & set(df.columns)	
	#sorted(list(tickers_found))

	# save the tickers
	#myString = ",".join(tickers_found)
	#np.savetxt(get_full_path_2_save(r'C:\phd\input\rpca', 'distance_tickers.csv'), ["%s" % myString], fmt='%s')
	
	#df_new = df[list(tickers_found)]
	
	max_rows = df.shape[0]
	
	root_dir = r"C:\phd\input\rpca\distances"
	for i in range(0, max_rows):
		if i < 2075:
			print('skipping .... %d' % (i))
			continue
			
		row = df.iloc[i]
		dt = df.index[i]
		filename_prefix = parser.parse(dt).strftime('%Y%m%d')
		tickers, D = get_distance_matrix(row)
		D.dump(get_full_path_2_save(root_dir, filename_prefix + '_D.dat'))

		# save the tickers
		myString = ",".join(tickers)
		np.savetxt(get_full_path_2_save(root_dir, filename_prefix + '_tickers.csv'), ["%s" % myString], fmt='%s')

		s = '%d.) Processed date = %s, # of tickers = %d' % (i, dt, D.shape[1])
		print(s)	
