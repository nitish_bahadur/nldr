import numpy as np
import pandas as pd

def get_svd_dimension(S, threshold=0.9):
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    cum_s_exp = np.cumsum(s_exp)        
    for i in range(0, cum_s_exp.shape[0]):        
        if cum_s_exp[i] >= threshold:
            return i
    return cum_s_exp.shape[0]    

def get_eig_dimension(S, threshold=0.9):
    """Calculate the dimension based on cumulative eigenvalue value percentages"""
    return get_svd_dimension(np.square(S), 0.9)

def get_svd_dim_gt_theta(S, theta = 0.01):
    """Calculate the dimension based on cumulative singular value percentages"""
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    s_gt_theta = [i for i in s_exp if i >= theta]
    return len(s_gt_theta)

def get_eig_dim_gt_theta(S, theta = 0.01):
    return get_svd_dim_gt_theta(np.square(S), theta)
