#!/bin/env python3
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -p short
#SBATCH -t 24:00:00

import numpy as np
import tensorflow as tf
import pandas as pd
import os
import csv
from datetime import datetime
import time
import sys
import logging


LAMBDA = 0.1
NUM_EPOCHS = 30000
BATCH_SIZE = 10
LR = 1e-4

def get_file_path(date_str):
    root_dir = r'../data/euclidean-distances/russell3000/w100'
    file_path = os.path.join(root_dir, date_str + '.mat')
    return file_path

def load_distance_matrix(date_str):
    file_path = get_file_path(date_str)
    if os.path.isfile(file_path):
        m = np.load(file_path)  
        m = m/np.max(m)
        return m
    else:
        return None

""" 
    The auto encoder with the following architecture:
    Input->100->100->100->100->60->100->100->100->100->Output
"""
def autoencoder(input_):
    # encoder
    en1 = tf.layers.dense(input_, 100, activation=tf.nn.sigmoid, name='layer1')     
    en2 = tf.layers.dense(en1, 100, activation=tf.nn.sigmoid, name='layer2')     
    en3 = tf.layers.dense(en2, 100, activation=tf.nn.sigmoid, name='layer3')     
    en4 = tf.layers.dense(en3, 100, activation=tf.nn.sigmoid, name='layer4')     

    encoded = tf.layers.dense(en4, 60, activation=None, name='layer5')

    # decoder
    de1 = tf.layers.dense(encoded, 100, activation=tf.nn.sigmoid, name='layer6')     
    de2 = tf.layers.dense(de1, 100, activation=tf.nn.sigmoid, name='layer7')     
    de3 = tf.layers.dense(de2, 100, activation=tf.nn.sigmoid, name='layer8')     
    de4 = tf.layers.dense(de3, 100, activation=tf.nn.sigmoid, name='layer9')     

    decoded = tf.layers.dense(de4, input_.shape[1], activation=None, name='layer10')  

    # loss is (output - input)^2
    regularization_penalty = LAMBDA * tf.norm(encoded, ord=1)
    loss = tf.losses.mean_squared_error(labels=input_, predictions=decoded) + regularization_penalty

    return encoded, decoded, loss, regularization_penalty


def main_driver(date_str):
    data = load_distance_matrix(date_to_process)
    if data is None:
        logging.debug('Input file for {0:s} not present'.format(date_to_process))  
    else:
        logging.debug('-----------------------------------------------------------')
        input_layer_size = data.shape[1]
        input_ = tf.placeholder(tf.float32, shape=[None, input_layer_size]) 

        # build the model
        # latent, output, loss
        encoded, decoded, loss, penalty = autoencoder(input_)

        # and we use the Adam Optimizer for training
        train = tf.train.AdamOptimizer(LR).minimize(loss)

        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
    
        train_loss_over_time = []
        penalty_loss_over_time = []
        for epoch in range(1, NUM_EPOCHS+1):
            train_loss = []
            penalty_loss = []
            for step in range(data.shape[0]//BATCH_SIZE):
                b_x = data[step*BATCH_SIZE: (step+1)*BATCH_SIZE,:]
                _, encoded_, decoded_, loss_, penalty_ = sess.run([train, encoded, decoded, loss, penalty], {input_: b_x})
                train_loss.append(loss_)
                penalty_loss.append(penalty_)
            train_loss_over_time.append(np.mean(train_loss))
            penalty_loss_over_time.append(np.mean(penalty_loss))

            if (epoch % 1000 == 0):    
                initial_train_loss = np.mean(train_loss_over_time[0:1000])                
                avg_train_loss = np.mean(train_loss_over_time)
                err_pct_reduced = (1 - (avg_train_loss/initial_train_loss))*100
                avg_penalty_loss = np.mean(penalty_loss_over_time)
                pct_loss_penalty = (avg_penalty_loss/avg_train_loss)*100
                logging.debug('@ Epoch: {0:d} penalty loss {1:.7f} is {2:.2f}% of total loss {3:.7f}. Error reduced: {4:.2f}%'.format(epoch, avg_penalty_loss, pct_loss_penalty, avg_train_loss, err_pct_reduced))
    
        encoded_.dump(r'../output/euclidean_dist_w100/lambda-0.1/' + date_str + '-LAMBDA-' + str(LAMBDA) + '.dat')
        x_test = np.reshape(data[99,:], (-1, 100))
        pred = sess.run(encoded, feed_dict={input_: x_test})
        np.savetxt(r'../output/euclidean_dist_w100/lambda-0.1/latent-' + date_str + '-LAMBDA-' + str(LAMBDA) + '.csv', pred, delimiter=',')

        sess.close()

if __name__ == '__main__':
    total_time = time.time()
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    date_to_process = sys.argv[1]
    
    logging.basicConfig(filename=r'../logs/' + date_to_process + '.log', level=logging.DEBUG)
    logging.debug('Date to process: {}'.format(date_to_process))

    # call the main routine
    main_driver(date_to_process)

    logging.debug('{0:s} process completed! in {1:d} seconds.'.format(date_to_process, int(time.time() - total_time)))
