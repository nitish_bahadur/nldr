import numpy as np
import tensorflow as tf
import pandas as pd
import os
import csv
from datetime import datetime
import time
import sys
import logging

def dense(x, size, scope):
    return tf.contrib.layers.fully_connected(x, size, activation_fn=None, scope=scope)

def dense_batch_relu(x, is_training=True):
    with tf.variable_scope(scope):
        h1 = tf.contrib.layers.fully_connected(x, 128, activation_fn=None, scope='dense')
        h2 = tf.contrib.layers.batch_norm(h1, center=True, scale=True, is_training=is_training, scope='bn')
        return tf.nn.relu(h2, 'relu')

def get_dimension(data_, encoding_dim_, num_epochs_, batch_size_, learning_rate_, loss_threshold_):
    alpha = 0.2

    tf.reset_default_graph()

    # leaky_relu = lambda x: tf.maximum(x*alpha, x)

    input_layer_size = data_.shape[1]
    input_ = tf.placeholder(tf.float32, [None, input_layer_size]) 

    # regularizer = tf.contrib.layers.l2_regularizer(scale=0.1)

    # encoder
    en0 = tf.layers.dense(input_, 128, activation=tf.nn.relu, name='layer1')     
    # en0 = tf.layers.batch_normalization(en0, training=True)
    # en0 = leaky_relu(en0)


    encoded = tf.layers.dense(en0, encoding_dim_, activation=tf.nn.relu, name='layer2')
    # encoded = tf.layers.batch_normalization(encoded, training=True)
    # encoded = leaky_relu(encoded)

    # decoder
    de0 = tf.layers.dense(encoded, 128, name='layer3', activation=tf.nn.relu)     
    # de0 = tf.layers.batch_normalization(de0, training=True)
    # de0 = leaky_relu(de0)

    decoded = tf.layers.dense(de0, input_layer_size, activation=None, name='layer4')  

    # loss is (output - input)^2
    loss = tf.losses.mean_squared_error(labels=input_, predictions=decoded)
    # loss = loss + tf.losses.get_regularization_loss()


    # Introducing Batch Normalization
    with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        train = tf.train.AdamOptimizer(learning_rate_).minimize(loss)


    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    tf.logging.set_verbosity(tf.logging.ERROR)

    train_loss_over_time = []
    for epoch in range(1, num_epochs_+1):
        train_loss = []

        for step in range(data_.shape[0]//batch_size_):
            b_x = data_[step*batch_size_: (step+1)*batch_size_,:]
            _, encoded_, decoded_, loss_ = sess.run([train, encoded, decoded, loss], {input_: b_x})
            train_loss.append(loss_)
        train_loss_over_time.append(np.mean(train_loss))

        

        avg_loss = np.mean(train_loss_over_time)
        if avg_loss <= loss_threshold_:
            print('    @ Epoch: {0:d} average loss {1:.7f} is less than equal to threshold of {2:s}'.format(epoch, avg_loss, str(loss_threshold_)))
            # logging.debug('    @ Epoch: {0:d} average loss {1:.7f} is less than equal to threshold of {2:s}'.format(epoch, avg_loss, str(loss_threshold_)))
            break
        else:
            if (epoch % 1000 == 0):    
                print('    @ Epoch: {0:d} average loss is {1:.7f}'.format(epoch, avg_loss))
                # logging.debug('    @ Epoch: {0:d} average loss is {1:.7f}'.format(epoch, avg_loss))
    
    if avg_loss > loss_threshold_:
        print('    @ Epoch: {0:d} average loss is {1:.7f}'.format(epoch, avg_loss))
        # logging.debug('    @ Epoch: {0:d} average loss is {1:.7f}'.format(epoch, avg_loss))

    sess.close()
    
    return (np.mean(train_loss_over_time), epoch)


def get_returns_daily_df(input_filepath):
    df = pd.read_csv(input_filepath)  
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
        
    df_ret = np.log(df / df.shift())
    return df_ret.iloc[1:]

def load_daily_prices(input_filepath):
    df = pd.read_csv(input_filepath)  
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)        
    return df

def calculate_daily_returns(df_):
    numerator = df_.iloc[1:] 
    denominator = df_.shift()
    df_ret = numerator / denominator.iloc[1:] 
    return df_ret

def get_next_encoding_dim(min_ae_dim=14, max_ae_dim=60):
    next_dim = int((max_ae_dim + min_ae_dim)*0.5)
    return next_dim




#--------------------------------------------------------------
# The main driver to calculate dimensionality of the market.
# -------------------------------------------------------------
if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    input_filepath = './data/russell3000-returns-financial-crisis-clean.csv'  # './data/russell3000.csv' # './data/equity-price.csv' 
    year = '2008-10'
    num_epochs = 10000
    batch_size = 10
    learning_rate = 0.001
    loss_threshold = 0.0000065

    # df = get_returns_daily_df(input_filepath)
    df = load_daily_prices(input_filepath)
    print('Prices loaded.  Data frame has {0:d} rows and {1:d} columns.'.format(df.shape[0], df.shape[1]))

    width = 60
    max_len = len(df) - width + 1

    csv_file = open('ae_dim_' + year +'.csv', mode='a', newline='')
    fieldnames = ['Date', 'Dimension', 'Loss']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
	
    for i in range(max_len):        
        data = df[i:i+width].copy()
        print('Data before dropna --> # of tickers: {0:d}. # of days: {1:d}'.format(data.shape[1], data.shape[0]))
        data.dropna(axis=1, inplace=True)
        print('Data now has # of tickers: {0:d}. # of days: {1:d}'.format(data.shape[1], data.shape[0]))

        ds = data.index[-1]
        date_str = datetime.strptime(ds, '%m/%d/%Y').strftime("%Y-%m-%d")
        # print('Loaded price for 61 days ending {0:s}'.format(date_str))
        
        # dt_ret = data # calculate_daily_returns(data)
        # ds = dt_ret.index[-1]
        # date_str = datetime.strptime(ds, '%m/%d/%Y').strftime("%Y-%m-%d")
        if date_str.startswith('2008-10-'):
            print('-----------------------------------------------------------')
            print('Now calculating dimension for {0:s}.  # of tickers: {1:d}. # of days: {2:d}'.format(ds, data.shape[1], data.shape[0]))
            # logging.debug('Now calculating dimension for {0:s}.  # of tickers: {1:d}. # of days: {2:d}'.format(ds, data.shape[1], data.shape[0]))
            training_batch = data.values

            max_ae_dim = 100
            min_ae_dim = 20
            encoding_dim = 0
            curr_dim = 0

            while True:
                encoding_dim = get_next_encoding_dim(min_ae_dim, max_ae_dim)
                print('    min_ae_dim:{0:d}, encoding_dim: {1:d},  max_ae_dim: {2:d}'.format(min_ae_dim, encoding_dim, max_ae_dim))
                # logging.debug('    min_ae_dim:{0:d}, encoding_dim: {1:d},  max_ae_dim: {2:d}'.format(min_ae_dim, encoding_dim, max_ae_dim))

                start_time = time.time()
                training_loss, epoch = get_dimension(training_batch, encoding_dim, num_epochs, batch_size, learning_rate, loss_threshold)
                print('    [%d],%.7f,%d,%d seconds'% (encoding_dim, training_loss, epoch, int(time.time() - start_time)))
                print('    ---------------------------------------------------')
                # logging.debug('    [%d],%.7f,%d,%d seconds\n'% (encoding_dim, training_loss, epoch, int(time.time() - start_time)))

                if (training_loss <= loss_threshold):
                    max_ae_dim = encoding_dim
                    curr_dim = encoding_dim
                else:
                    min_ae_dim = encoding_dim

                if (max_ae_dim - min_ae_dim) <= 1:
                    print('Exit while loop.')
                    break

            if curr_dim != 0:
                writer.writerow({'Date': ds, 'Dimension': curr_dim, 'Loss': '{0:.7f}'.format(training_loss)})
                csv_file.flush()
            else:
                writer.writerow({'Date': ds, 'Dimension': 120, 'Loss': '{0:.7f}'.format(100.0)})
                csv_file.flush()

    csv_file.close()
    print('{} process completed!'.format(year))
    # logging.debug('{} process completed!'.format(year))