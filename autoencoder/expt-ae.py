import numpy as np
import tensorflow as tf
import pandas as pd
import os
import csv
from datetime import datetime

# #### Load Input Data from a Matrix

# In[2]:


def load_training_batch(filename_, debug=True):
    if not debug:
        dir_path = os.path.join(os.path.sep, BASE_FOLDER, filename_)
        filepath = os.path.join(os.path.sep, dir_path, "train_batch.npy")
    else:
        filepath="train_batch.npy"
    training_batch_ = np.load(filepath)
    return training_batch_


# ### Auto Encoder's Architecture
# Let us initialize the auto encoder parameters
# training data
NUM_INPUT_TICKERS = 500
NUM_INSTANCES = 2000
BASE_FOLDER = r'C:\phd\ae\data\eoddata-training-500'

# Hyper Parameters
BATCH_SIZE = 100
NUM_EPOCHS = 150
# learning rate
LR = 0.001    


def get_dimension(date_str, encoding_dim_):
    tf.reset_default_graph()
    tf.set_random_seed(1)
    train_inputs = load_training_batch(date_str, False)
    input_ = tf.placeholder(tf.float32, [None, NUM_INPUT_TICKERS]) 

    # encoder
    en1 = tf.layers.dense(input_, 256, activation=tf.nn.tanh)     
    #en2 = tf.layers.dense(en1, 128, activation=tf.nn.tanh)
    #en3 = tf.layers.dense(en2, 64, activation=tf.nn.tanh)
    encoded = tf.layers.dense(en1, encoding_dim_, activation=tf.nn.tanh)

    # decoder
    de1 = tf.layers.dense(encoded, 256, activation=tf.nn.tanh)     
    #de2 = tf.layers.dense(de1, 128, activation=tf.nn.tanh)
    #de3 = tf.layers.dense(de2, 256, activation=tf.nn.tanh)
    decoded = tf.layers.dense(de1, NUM_INPUT_TICKERS, activation=None)  #tf.nn.tanh

    # loss is (output - input)^2
    loss = tf.losses.mean_squared_error(labels=input_, predictions=decoded)
    train = tf.train.AdamOptimizer(LR).minimize(loss)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    tf.logging.set_verbosity(tf.logging.ERROR)

    train_loss_over_time = []
    for epoch in range(NUM_EPOCHS):
        train_loss = []

        for step in range(train_inputs.shape[0]//BATCH_SIZE):
            b_x = train_inputs[step*BATCH_SIZE: (step+1)*BATCH_SIZE,:]
            _, encoded_, decoded_, loss_ = sess.run([train, encoded, decoded, loss], {input_: b_x})
            train_loss.append(loss_)
        train_loss_over_time.append(np.mean(train_loss))
            
    sess.close()
    
    return np.mean(train_loss_over_time)

def get_filenames_using_dates():
    df = pd.read_csv(r'C:\phd\ae\data\equity-price.csv')  # C:\phd\ae\data\equity-price.csv #C:\phd\ae\data\russell3000.csv
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    df_ret = df_ret.iloc[1:]
    filenames = [datetime.strptime(dt_str, '%m/%d/%Y').strftime("%Y-%m-%d") for dt_str in df_ret.index.tolist()]
    return filenames


#--------------------------------------------------------------
# The main driver to calculate dimensionality of the market.
# -------------------------------------------------------------
if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    filenames = get_filenames_using_dates()
    csv_file = open('ae_dim_500.csv', mode='a', newline='')
    fieldnames = ['Date', 'Dimension', 'Loss']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
	
    for filename in filenames:
        print('-----------------------------------------------------------')
        print('Now calculating dimension for %s'% filename)
        training_batch = load_training_batch(filename, False)
        for encoding_dim in range(15, 80):
            training_loss = get_dimension(filename, encoding_dim)
            print('%d,%.7f'% (encoding_dim, training_loss))
            writer.writerow({'Date': filename, 'Dimension': encoding_dim, 'Loss': '{0:0.7f}'.format(training_loss)})
    csv_file.close()
    print('Process completed!')
	
	
"""    
filename = "2017-10-06"
    print('-----------------------------------------------------------')
    print('Now calculating dimension for %s' % filename)
    training_batch = load_training_batch(filename, False)
    for encoding_dim in range(50, 100):
        training_loss = get_dimension(filename, encoding_dim)
        print('%d,%.7f' % (encoding_dim, np.mean(training_loss)))
"""