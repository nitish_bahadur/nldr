import numpy as np
import tensorflow as tf
import pandas as pd
import os
import csv
from datetime import datetime
import time
import sys
import logging

def get_dimension(data_, encoding_dim_, num_epochs_, batch_size_, learning_rate_, loss_threshold_):
    tf.reset_default_graph()
    tf.logging.set_verbosity(tf.logging.ERROR)

    input_layer_size = data_.shape[1]
    input_ = tf.placeholder(tf.float32, [None, input_layer_size]) 

    # encoder
    en0 = tf.layers.dense(input_, 60, activation=tf.nn.relu, name='layer1')     

    encoded = tf.layers.dense(en0, encoding_dim_, activation=tf.nn.relu, name='layer2')

    # decoder
    de0 = tf.layers.dense(encoded, 60, name='layer3', activation=tf.nn.relu)     

    decoded = tf.layers.dense(de0, input_layer_size, activation=None, name='layer4')  

    # loss is (output - input)^2
    loss = tf.losses.mean_squared_error(labels=input_, predictions=decoded)

    train = tf.train.AdamOptimizer(learning_rate_).minimize(loss)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    train_loss_over_time = []
    found_dimension = False

    for epoch in range(1, num_epochs_+1):
        train_loss = []

        for step in range(data_.shape[0]//batch_size_):
            b_x = data_[step*batch_size_: (step+1)*batch_size_,:]
            _, encoded_, decoded_, loss_ = sess.run([train, encoded, decoded, loss], {input_: b_x})
            train_loss.append(loss_)
        train_loss_over_time.append(np.mean(train_loss))

        # check early stopping

        avg_loss = np.mean(train_loss_over_time)
        if avg_loss <= loss_threshold_:
            print('    @ Epoch: {0:d} average loss {1:.7f} is less than equal to threshold of {2:s}'.format(epoch, avg_loss, str(loss_threshold_)))            
            found_dimension = True
            break
        else:
            if (epoch % 1000 == 0):    
                print('    @ Epoch: {0:d} average loss is {1:.7f}'.format(epoch, avg_loss))
    
    if not found_dimension:
        print('    @ Epoch: {0:d} average loss is {1:.7f}.'.format(epoch, avg_loss))

    sess.close()
    
    return (np.mean(train_loss_over_time), epoch)


def load_daily_prices(input_filepath):
    df = pd.read_csv(input_filepath)  
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)        
    return df



def get_file_path(ds):
    root_dir = r'./data/euclidean-distances/russell3000'
    file_path = os.path.join(root_dir, ds + '.mat')
    return file_path

def load_distances(ds):
    file_path = get_file_path(ds)
    if os.path.isfile(file_path):
        m = np.load(file_path)  
        return m
    else:
    	return None

def get_next_encoding_dim(min_ae_dim=14, max_ae_dim=60):
    next_dim = int((max_ae_dim + min_ae_dim)*0.5)
    return next_dim




#--------------------------------------------------------------
# The main driver to calculate dimensionality of the market.
# -------------------------------------------------------------
if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    input_filepath = './data/russell3000.csv'  # './data/russell3000.csv' # './data/equity-price.csv' 
    # year = '2008-10'
    num_epochs = 15000
    batch_size = 10
    learning_rate = 0.001
    loss_threshold = 0.001
    max_dimensions = 60
    min_dimensions = 4

    # df = get_returns_daily_df(input_filepath)
    df = load_daily_prices(input_filepath)
    print('Prices loaded.  Data frame has {0:d} rows and {1:d} columns.'.format(df.shape[0], df.shape[1]))

    width = 60
    max_len = len(df) - width + 1

    csv_file = open('ae_dim_dist' + '.csv', mode='a', newline='')
    fieldnames = ['Date', 'Dimension', 'Loss']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
	
    for i in range(max_len):        
        data_temp = df[i:i+width].copy()
        ds = data_temp.index[-1]
        date_str = datetime.strptime(ds, '%m/%d/%Y').strftime("%Y-%m-%d")
        data = load_distances(date_str)
        if data is None:
            print('Input file for {0:s} not present'.format(date_str))	
            continue

        if date_str.startswith('2008-10-') or date_str.startswith('2008-09-'):
	        print('-----------------------------------------------------------')
	        print('Now calculating dimension for {0:s}.  # of tickers: {1:d}. # of days: {2:d}'.format(ds, data.shape[1], data.shape[0]))
	        training_batch = data/np.max(data)

	        max_ae_dim = max_dimensions
	        min_ae_dim = min_dimensions
	        encoding_dim = 0
	        curr_dim = 0

	        while True:
	            encoding_dim = get_next_encoding_dim(min_ae_dim, max_ae_dim)
	            print('    min_ae_dim:{0:d}, encoding_dim: {1:d},  max_ae_dim: {2:d}'.format(min_ae_dim, encoding_dim, max_ae_dim))

	            start_time = time.time()
	            training_loss, epoch = get_dimension(training_batch, encoding_dim, num_epochs, batch_size, learning_rate, loss_threshold)
	            print('    [%d],%.7f,%d,%d seconds'% (encoding_dim, training_loss, epoch, int(time.time() - start_time)))
	            print('    ---------------------------------------------------')

	            if (training_loss <= loss_threshold):
	                max_ae_dim = encoding_dim
	                curr_dim = encoding_dim
	            else:
	                min_ae_dim = encoding_dim

	            if (max_ae_dim - min_ae_dim) <= 1:
	                print('Exit while loop.')
	                break

	        if curr_dim != 0:
	            writer.writerow({'Date': ds, 'Dimension': curr_dim, 'Loss': '{0:.7f}'.format(training_loss)})
	            csv_file.flush()
	        else:
	            writer.writerow({'Date': ds, 'Dimension': max_dimensions, 'Loss': '{0:.7f}'.format(999.0)})
	            csv_file.flush()

    csv_file.close()
    print('process completed!')
    # logging.debug('{} process completed!'.format(year))