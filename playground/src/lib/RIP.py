import numpy as np
import matplotlib.pylab as py

def fold(x):
    n = len(x)
    y = np.empty([n],dtype=int)
    for i in range(n/2):
        y[2*i] = x[i]
        y[2*i+1] = x[n-i-1]
    return y
    
def cut(x):
    n = len(x)
    y = np.empty([n],dtype=int)
    for i in range(n/2):
        y[2*i] = x[i]
        y[2*i+1] = x[n/2+i]
    return y

def cutFold(x):
    return cut(fold(x))

def foldCut(x):
    return fold(cut(x))

def shift(x,k=1):
    n = len(x)
    y = np.empty([n],dtype=int)
    for i in range(n):
        y[i] = x[(i+k)%n]
    return y

def shiftFold(x,k=1):
    return shift(fold(x))

def shiftCut(x,k=1):
    return shift(cut(x))

def iterate(x,f=fold):
    n = len(x)
    orig = x.copy()
    X = []
    X.append(x)
    x = f(x)
    while not np.array_equal(x,orig):
        X.append(x)
        x = f(x)
    return np.matrix(X).T
                
def binPrint(x):
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            if x[i,j] == 1:
                print '+',
            else:
                print '-',
        print
        
def orbit(x,printOrbit=True,f=fold):
    x = np.array(x)
    X = iterate(x,f=f)
    if printOrbit:
        print 'orbit'
        binPrint(X)
    print 'size',X.shape[1]
    print 'dot products'
    d = []
    for i in range(X.shape[1]):
        for j in range(i+1,X.shape[1]):
            d.append(int(X[:,i].T*X[:,j]))
    py.hist(d)

class randomVector(object):
    def __init__(self, m, maxCalls=1000):
        self.m = m
        self.calls = 0
        self.maxCalls = maxCalls
            
    def __call__(self):
        if self.calls > self.maxCalls:
            return None
        r = np.matrix(np.random.binomial(1,0.5,size=[self.m,1]))*2-1
        # Normalize so that first row is 1
        r[0,0] = 1
        self.calls += 1
        return r

    def reset(self):
        self.calls = 0 

class moduloVector(object):
    def __init__(self, m, s=3):
        self.m = m
        self.s = s
        self.r = 1
            
    def __call__(self):
        self.r = (self.r*self.s)%(2**m)
        r = np.matrix(np.zeros([self.m,1]))
        for i in range(self.m):
            # FIXME:  Code is not complete
            pass
        # Normalize so that first row is 1
        r[0,0] = 1
        return r

    def reset(self):
        self.r = 1 
    
class constructor(object):
    def __init__(self, m, n):
        self.m = m
        self.n = n
        self.A = np.matrix(np.ones([m,n]))
        self._lastRandom = 0
        self.v = randomVector(self.m)
        
    def solve(self, errors=2, maxRestarts=100):
        restarts = 0
        ok = 1
        while ok < self.n:
            # Get a new vector to test
            t = self._nextVector()
            # If this function returns None, the we are out of candidate
            # vectors and need to reset
            if t is None:
                print "resteting %d times, made it to %d"%(restarts,ok)
                ok = self._reset()
                restarts +=1
                self.v.reset()
                t = self._nextVector()
            
            # Test all of the current columns
            good = True
            for i in range(ok):
                if np.abs(self.A[:,i].T*t) > errors:
                    good = False
                    break
            if good:
                self.A[:,ok] = t
                ok += 1
            if restarts > maxRestarts:
                break
        return self.A
    
    def _nextVector(self):
        return self.v()
        
    def _reset(self):
        # We start with a matrix of all ones
        self.A = np.matrix(np.ones([self.m,self.n]))
        # By normalization, the first column is ok
        ok = 1
        return ok
    

def test():
    x=np.array([1,2,3,4,5,6,7,8])
    print 'orig'
    print x
    print 'fold'
    print fold(x)
    print 'cut'
    print cut(x)
    print 'shift'
    print shift(x)
    print 'shiftFold'
    print shiftFold(x)

if __name__=='__main__':
    test()
