﻿import numpy as np
import os
import sys
import pandas as pd
from dateutil import parser
import scipy.sparse
from dimredu.denseSolvers import denseToSparse as denseToSparse
from dimredu.sRPCAviaADMMFast import sRPCA as sRPCA

def get_returns_daily_df(filename):
    """Load the data and calculate log return.  Remove the first row because it will always be NaN."""
    df = pd.read_csv(filename) # 'russell3000_eod_prices.csv'
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    return df_ret.ix[1:]
	
def get_svd_dimension(S, threshold=0.9):
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    cum_s_exp = np.cumsum(s_exp)        
    for i in range(0, cum_s_exp.shape[0]):
        if cum_s_exp[i] >= threshold:
            return i
    return cum_s_exp.shape[0]    

def get_eig_dimension(S, threshold=0.9):
    """Calculate the dimension based on cumulative eigenvalue value percentages"""
    return get_svd_dimension(np.square(S), 0.9)

def get_svd_dim_gt_theta(S, theta = 0.01):
    """Calculate the dimension based on cumulative singular value percentages"""
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    s_gt_theta = [i for i in s_exp if i >= theta]
    return len(s_gt_theta)

def get_eig_dim_gt_theta(S, theta = 0.01):
    return get_svd_dim_gt_theta(np.square(S), theta)

def get_full_path_2_save(root_dir, filename):
    full_path = os.path.join(root_dir, filename)    
    return full_path	

def get_scaled_lambda(m, n, _lambda):
    #lam = 1. / np.sqrt(np.max([m, n]))
    lam = 1. / np.sqrt(2396.58)
    return lam * _lambda
	
def calc_dim_rpca_ts(df, width, threshold, _lambda, maxIteration, theta, root_dir):
	"""Loop through the data frame and then call Robust PCA"""
	max_rows = df.shape[0] - (width + 1)    
	dates = []
	lambdas = []
	lambdas_scaled = []
	filenames = []

	rpca_svd_dimensions = []
	rpca_eig_dimensions = []
	rpca_svd_gt_1pct = []
	rpca_eig_gt_1pct = []

	# _lambda = 0.35
	# maxIteration = 250
	# theta = 0.01

	epsilon1 = 1e-4
	epsilon2 = 1e-3
	
	for i in range(0, max_rows):
		M = df[i:i+width].copy()
		orig_cols_len = len(M.columns)
		M.dropna(axis=1, inplace=True)
		new_cols_len = len(M.columns)
		if new_cols_len >= 2000:
			dt = df.index[i+width]
			filename_prefix = parser.parse(dt).strftime('%Y%m%d')
			
			
			# call dense solver here and get the input matrices
			Err = np.ones(M.shape)*1e-4
			m, n, u, v, vecM, vecEpsilon = denseToSparse(M.as_matrix(), Err)
			maxRank = np.min(M.shape)
			
			scaled_lambda = get_scaled_lambda(M.shape[0], M.shape[1], _lambda)

			s = '%d.) Now processing date = %s with _lambdaScaler = %s and scaled lambda = %5f' % (i, dt, str(_lambda), scaled_lambda)
			print(s)
			
			# Robust PCA
			U, E, VT, S, B = sRPCA(m, n, u, v, vecM, vecEpsilon, maxRank, lam=_lambda, 
								   mu=None, rho=None, epsilon1=epsilon1, epsilon2=epsilon2,
								   truncateK=0, SOff=False,
								   maxIteration=maxIteration, verbose=False)
				  
			svd_dim = get_svd_dimension(E, threshold)
			eig_dim = get_eig_dimension(E, threshold)
			svd_gt1pct_dim = get_svd_dim_gt_theta(E, theta)
			eig_gt1pct_dim = get_eig_dim_gt_theta(E, theta)
			
			dates.append(dt)
			lambdas.append(_lambda)
			lambdas_scaled.append(scaled_lambda)
			filenames.append(filename_prefix)

			
			rpca_svd_dimensions.append(svd_dim)        
			rpca_eig_dimensions.append(eig_dim)
			rpca_svd_gt_1pct.append(svd_gt1pct_dim)
			rpca_eig_gt_1pct.append(eig_gt1pct_dim)

			# Store the matrix is a directory
			# Write the suffix to the top a file
			
			# Data is always written in ‘C’ order, independent of the order of matrix. 
			# The data produced by this method can be recovered using the function fromfile().
			U.dump(get_full_path_2_save(root_dir, filename_prefix + '_U.dat'))
			E.dump(get_full_path_2_save(root_dir, filename_prefix + '_E.dat'))
			VT.dump(get_full_path_2_save(root_dir, filename_prefix + '_VT.dat'))
			scipy.sparse.save_npz(get_full_path_2_save(root_dir, filename_prefix + '_S.npz'), S) # compressed matrix
			scipy.sparse.save_npz(get_full_path_2_save(root_dir, filename_prefix + '_B.npz'), B) # compressed matrix

			# save the tickers
			myList = M.columns.tolist()
			myString = ",".join(myList )
			np.savetxt(get_full_path_2_save(root_dir, filename_prefix + '_tickers.csv'), ["%s" % myString], fmt='%s')

	df1 = pd.DataFrame()
	df1['Dates'] = dates
	df1['LAMBDA'] = lambdas
	df1['LAMBDA_SCALED'] = lambdas_scaled
	df1['FILENAME_PREFIX'] = filenames
	df1['RPCA_SVD_DIMS'] = rpca_svd_dimensions
	df1['RPCA_EIG_DIMS'] = rpca_eig_dimensions
	df1['RPCA_SVD_GT_1PCT_DIMS'] = rpca_svd_gt_1pct
	df1['RPCA_EIG_GT_1PCT_DIMS'] = rpca_eig_gt_1pct

	return df1

def get_df_result_filename(lam):
    full_path = r'C:\phd\output\rpca\lambda_' + str(lam) + r'\russell3000_returns_dim.csv'    
    return full_path	
	
def get_csv_root_dir(lam):
    full_path = r'C:\phd\output\rpca\lambda_' + str(lam) + r'\returns'    
    return full_path	

if __name__ == "__main__":
    #lam_list = [0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.15]
	#lam_list = [1, 2, 4, 8, 16, 32, 64, 128]
	#lam_list = [0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625]
	lam_list = [0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625]
	INPUT_FILE = r"C:\phd\input\russell3000_eod.csv"
	df = get_returns_daily_df(INPUT_FILE)
	df = df.round(7)
	
	width = 60
	threshold = 0.9
	maxIteration = 500
	theta = 0.01
	
	for i in range(0, len(lam_list)):
		_lambda = lam_list[i]
		_df_result_filename = get_df_result_filename(_lambda)
		_root_dir = get_csv_root_dir(_lambda)
		
		df_result = calc_dim_rpca_ts(df, width, threshold, _lambda, maxIteration, theta, _root_dir)
		df_result.to_csv(_df_result_filename)
	
