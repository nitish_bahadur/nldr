#!/bin/env python3
#SBATCH -N 1 # nodes requested
#SBATCH -n 1 # tasks requested
#SBATCH -c 1 # cores requested
#SBATCH --gres=gpu:1 # nos GPUS
#SBATCH -t 2:00:00 # time requested in hour:minute:second

from keras.layers import Input, Dense
from keras.models import Model
from keras import losses
from keras import optimizers
from keras import regularizers
import tensorflow as tf
import numpy as np
import os

from datetime import datetime
import time
import sys
import logging

import json
from keras.callbacks import Callback

L1 = 1e-7
NUM_EPOCHS = 40000
BATCH_SIZE = 10
LR = 1e-5
INPUT_DIR = r'../data/spy_eod/returns'
OUTPUT_DIR = r'../data/spy_eod/results/returns'

def get_file_path(root_dir, date_str, img_type=None):
    if img_type is None:
        file_path = os.path.join(root_dir, date_str + '.dat')
    else:
        file_path = os.path.join(root_dir, date_str + img_type + '.dat')
    return file_path

def load_data(date_str):
    file_path = get_file_path(INPUT_DIR, date_str)
    m = np.load(file_path)  
    return m

def save_history(history, filename):
    with open(filename, 'w') as f:
        json.dump(history.history, f)


# ------------------------------------------------------------------------
# A custom class that implements exiting early stopping based on loss
# value.  We want to run this by default till 1e-6
# ------------------------------------------------------------------------
class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_loss', value=1e-5, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            logging.debug("Early stopping requires %s available!" % self.monitor, RuntimeWarning)

        if current < self.value:
            if self.verbose > 0:
                logging.debug("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True


# average the z values across the day
# see if the z_i >= threshold, in this 
# case 1%, we expect mean z is sent in
def count_gt_threshold(z, threshold):
    tot = sum(z)
    z_pct = [(i/tot) for i in sorted(z, reverse=True)]
    z_gt_theta = [i for i in z_pct if i >= threshold]
    return len(z_gt_theta)

# start adding the largest values first; keep adding 
# till you get 90%
def count_upto_threshold(z, threshold):
    tot = sum(z)
    z_pct = [(i/tot) for i in sorted(z, reverse=True)]
    cum_z_pct = np.cumsum(z_pct)
    for i in range(0, cum_z_pct.shape[0]):
        if cum_z_pct[i] >= 0.9:
            return i
    return cum_z_pct.shape[0]

""" 
    This the simplest auto encoder.  
"""
def build_model_0(input_dim):
    # encoder
    input_img = Input(shape=(input_dim,))
    encoded = Dense(512, activation='relu')(input_img)
    encoded = Dense(256, activation='relu')(encoded)
    encoded = Dense(128, activation='relu')(encoded)

    # using l1 regularizer
    encoded = Dense(64, activation='sigmoid', activity_regularizer=regularizers.l1(L1))(encoded)

    # decoder
    decoded = Dense(128, activation='relu')(encoded)
    decoded = Dense(256, activation='relu')(decoded)
    decoded = Dense(512, activation='relu')(decoded)

    decoded = Dense(input_dim, activation='tanh')(decoded)

    encoder = Model(input_img, encoded)
    autoencoder = Model(input_img, decoded)
    
    return encoder, autoencoder

# build the deep auto encoder model
def get_model(input_dim):
    logging.debug('build_model_0 512 --> 256 --> 128 --> 64 --> 128 --> 256 --> 512  will be used.')
    encoder, autoencoder = build_model_0(input_dim)
    return encoder, autoencoder

# the main routine that drives all the processing
def main_driver(date_to_process):
    x_train = load_data(date_to_process)
    x_train = x_train[:,:-2]
    x_train = x_train.astype('float32') / (np.max(x_train) - np.min(x_train))
    x_test = x_train

    logging.debug('INPUT_DIR: {0:s}'.format(INPUT_DIR)) 
    logging.debug('OUTPUT_DIR: {0:s}'.format(OUTPUT_DIR)) 
    logging.debug('L1 Regularizer: {0:s}'.format(str(L1))) 
    logging.debug('Learning Rate: {0:s}'.format(str(LR))) 
    logging.debug('NUM_EPOCHS: {}'.format(str(NUM_EPOCHS))) 
    # logging.debug('Running autoencoder for S&P 500 with l1 regularizers LR: {}'.format(str(LR)))

    if x_train is None:
        logging.debug('Input file for {0:s} not present'.format(date_to_process))  
    else:        
        logging.debug('Input file to process: {0:s}'.format(date_to_process))  
        logging.debug('Shape of input file: ({0:d}, {1:d})'.format(x_train.shape[0], x_train.shape[1]))

        # use even number of features so that we can plot
        # things in a matrix if required
        input_dim = x_train.shape[1]

        encoder, autoencoder = get_model(input_dim)

        autoencoder.compile(loss=losses.mean_squared_error,
                            optimizer=optimizers.Adam(lr=LR, epsilon=1e-08))

        filename = os.path.join(OUTPUT_DIR, date_to_process + '_history' + '.json')

        callbacks = [
            EarlyStoppingByLossVal(monitor='loss', value=1e-7, verbose=1)
        ]

        history = autoencoder.fit(x_train, x_train, 
                        epochs=NUM_EPOCHS, 
                        batch_size=BATCH_SIZE, 
                        shuffle=False,
                        verbose=2,
                        callbacks=callbacks)

        encoded_imgs = encoder.predict(x_test)
        decoded_imgs = autoencoder.predict(x_test)

        logging.debug('x_test min: {0:f} max: {1:f}'.format(np.min(x_test), np.max(x_test)))
        logging.debug('encoded_imgs min: {0:f} max: {1:f}'.format(np.min(encoded_imgs), np.max(encoded_imgs)))
        logging.debug('decoded_imgs min: {0:f} max: {1:f}'.format(np.min(decoded_imgs), np.max(decoded_imgs)))
        logging.debug('final loss: {:.8f}'.format(history.history['loss'][-1]))
        
        z = np.mean(encoded_imgs, axis=0)
        logging.debug('Dimension based on 1%: {} '.format(count_gt_threshold(z, 0.01)))
        logging.debug('Dimension based upto 90%: {} '.format(count_upto_threshold(z, 0.9)))

        # save the output files
        encoded_imgs.dump(get_file_path(OUTPUT_DIR, date_to_process, '_encoded'))
        # decoded_imgs.dump(get_file_path(OUTPUT_DIR, date_to_process, '_decoded'))
        save_history(history, filename)

if __name__ == '__main__':
    total_time = time.time()
    tf.logging.set_verbosity(tf.logging.ERROR)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    date_to_process = sys.argv[1]
    
    logging.basicConfig(filename=r'../logs/' + date_to_process + '.log', level=logging.DEBUG)
    logging.debug('Date to process: {}'.format(date_to_process))

    # call the main routine
    main_driver(date_to_process)

    logging.debug('{0:s} process completed! in {1:d} seconds.'.format(date_to_process, int(time.time() - total_time)))
