﻿import numpy as np
import os
import sys
import pandas as pd
from dateutil import parser
import scipy.sparse

def get_returns_daily_df(filename):
    """Load the data and calculate log return.  Remove the first row because it will always be NaN."""
    df = pd.read_csv(filename) # 'russell3000_eod_prices.csv'
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    return df_ret.ix[1:]
	
def get_full_path_2_save(root_dir, filename):
    full_path = os.path.join(root_dir, filename)    
    return full_path	
				
if __name__ == "__main__":
	INPUT_FILE = r"C:\phd\input\russell3000_eod_prices.csv"
	df = get_returns_daily_df(INPUT_FILE)
	df = df.round(7)
	width = 60
	max_rows = df.shape[0] - (width + 1)    
	
	root_dir = r'C:\phd\output\rpca\lambda_0.05\returns'
	for i in range(0, max_rows):
		M = df[i:i+width].copy()
		M.dropna(axis=1, inplace=True)
		dt = df.index[i+width]
		filename_prefix = parser.parse(dt).strftime('%Y%m%d')
		
		myList = M.columns.tolist()
		myString = ",".join(myList )
		
		np.savetxt(get_full_path_2_save(root_dir, filename_prefix + '_tickers.csv'), ["%s" % myString], fmt='%s')
		
	print('Done!')