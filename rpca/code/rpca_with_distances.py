﻿import numpy as np
import os
import sys
import pandas as pd
from dateutil import parser
from numpy.linalg import norm
import scipy.sparse
from dimredu.denseSolvers import denseToSparse as denseToSparse
from dimredu.sRPCAviaADMMFast import sRPCA as sRPCA

def get_returns_daily_df(filename):
    """Load the data and calculate log return.  Remove the first row because it will always be NaN."""
    df = pd.read_csv(filename)
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)
    df_ret = np.log(df / df.shift())
    return df_ret.ix[1:]
	
def get_svd_dimension(S, threshold=0.9):
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    cum_s_exp = np.cumsum(s_exp)        
    for i in range(0, cum_s_exp.shape[0]):
        if cum_s_exp[i] >= threshold:
            return i
    return cum_s_exp.shape[0]    

def get_eig_dimension(S, threshold=0.9):
    """Calculate the dimension based on cumulative eigenvalue value percentages"""
    return get_svd_dimension(np.square(S), 0.9)

def get_svd_dim_gt_theta(S, theta = 0.01):
    """Calculate the dimension based on cumulative singular value percentages"""
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    s_gt_theta = [i for i in s_exp if i >= theta]
    return len(s_gt_theta)

def get_eig_dim_gt_theta(S, theta = 0.01):
    return get_svd_dim_gt_theta(np.square(S), theta)

def get_full_path_2_save(root_dir, filename):
    full_path = os.path.join(root_dir, filename)    
    return full_path	

def get_scaled_lambda(m, n, _lambda):
    lam = 1. / np.sqrt(np.max([m, n]))
    return lam * _lambda


def get_distance_matrix(df):
    distance = None
    rows = len(df)
    distance = np.zeros((rows, rows))    
    for i, rowi in df.iterrows():
        for j, rowj in df.iterrows():
            distance[i,j] = norm(rowi - rowj)            
    return distance	
	
	
def calc_dim_rpca_ts(df, width, threshold, _lambda, maxIteration, theta, root_dir):
	"""Loop through the data frame and then call Robust PCA"""
	max_rows = df.shape[0] - width    
	dates = []
	lambdas = []
	lambdas_scaled = []
	filenames = []

	rpca_svd_dimensions = []
	rpca_eig_dimensions = []
	rpca_svd_gt_1pct = []
	rpca_eig_gt_1pct = []

	epsilon1 = 1e-5
	epsilon2 = 1e-4
	for i in range(0, max_rows):
		M = df[i:i+width].copy()
		orig_cols_len = len(M.columns)
		M.dropna(axis=1, inplace=True)
		new_cols_len = len(M.columns)

		dt = df.index[i+width]
		filename_prefix = parser.parse(dt).strftime('%Y%m%d')
		
		# -----------------------------------------------------------
		# dates are indexes, the distance matrix needs integers to 
		# go through all this
		# -----------------------------------------------------------
		df2 = M.reset_index()
		df2.drop('Dates', axis=1, inplace=True)
		D = get_distance_matrix(df2)
		
		# call dense solver here and get the input matrices
		Err = np.ones(D.shape)*1e-5
		m, n, u, v, vecM, vecEpsilon = denseToSparse(D, Err)
		maxRank = np.min(D.shape)
		
		scaled_lambda = get_scaled_lambda(D.shape[0], D.shape[1], _lambda)

		s = '%d.) Now processing date = %s with _lambdaScaler = %s and scaled lambda = %5f' % (i, dt, str(_lambda), scaled_lambda)
		print(s)
		
		# Robust PCA
		U, E, VT, S, B = sRPCA(m, n, u, v, vecM, vecEpsilon, maxRank, lam=_lambda, 
							   mu=None, rho=None, epsilon1=epsilon1, epsilon2=epsilon2,
							   truncateK=0, SOff=False,
							   maxIteration=maxIteration, verbose=False)
			  
		svd_dim = get_svd_dimension(E, threshold)
		eig_dim = get_eig_dimension(E, threshold)
		svd_gt1pct_dim = get_svd_dim_gt_theta(E, theta)
		eig_gt1pct_dim = get_eig_dim_gt_theta(E, theta)
		
		dates.append(dt)
		lambdas.append(_lambda)
		lambdas_scaled.append(scaled_lambda)
		filenames.append(filename_prefix)

		
		rpca_svd_dimensions.append(svd_dim)        
		rpca_eig_dimensions.append(eig_dim)
		rpca_svd_gt_1pct.append(svd_gt1pct_dim)
		rpca_eig_gt_1pct.append(eig_gt1pct_dim)

		# Store the matrix is a directory
		# Write the suffix to the top a file
		
		# Data is always written in ‘C’ order, independent of the order of matrix. 
		# The data produced by this method can be recovered using the function fromfile().
		U.dump(get_full_path_2_save(root_dir, filename_prefix + '_U.dat'))
		E.dump(get_full_path_2_save(root_dir, filename_prefix + '_E.dat'))
		VT.dump(get_full_path_2_save(root_dir, filename_prefix + '_VT.dat'))
		scipy.sparse.save_npz(get_full_path_2_save(root_dir, filename_prefix + '_S.npz'), S) # compressed matrix
		scipy.sparse.save_npz(get_full_path_2_save(root_dir, filename_prefix + '_B.npz'), B) # compressed matrix

		# save the tickers
		myList = M.columns.tolist()
		myString = ",".join(myList )
		np.savetxt(get_full_path_2_save(root_dir, filename_prefix + '_tickers.csv'), ["%s" % myString], fmt='%s')

	df1 = pd.DataFrame()
	df1['Dates'] = dates
	df1['LAMBDA'] = lambdas
	df1['LAMBDA_SCALED'] = lambdas_scaled
	df1['FILENAME_PREFIX'] = filenames
	df1['RPCA_SVD_DIMS'] = rpca_svd_dimensions
	df1['RPCA_EIG_DIMS'] = rpca_eig_dimensions
	df1['RPCA_SVD_GT_1PCT_DIMS'] = rpca_svd_gt_1pct
	df1['RPCA_EIG_GT_1PCT_DIMS'] = rpca_eig_gt_1pct

	return df1

def get_df_result_filename(lam):
    full_path = r'C:\phd\output\rpca\sp500\lambda_' + str(lam) + r'\width_120\sp500_distances_width_120_dim.csv'    
    return full_path	
	
def get_csv_root_dir(lam):
    full_path = r'C:\phd\output\rpca\sp500\lambda_' + str(lam) + r'\width_120\distances'    
    return full_path	

if __name__ == "__main__":
	lam_list = [1]
	INPUT_FILE = r"C:\phd\input\rpca\sp500_prices.csv"
	df = get_returns_daily_df(INPUT_FILE)
	df = df.round(7)
	
	width = 120
	threshold = 0.9
	maxIteration = 500
	theta = 0.01
	
	for i in range(0, len(lam_list)):
		_lambda = lam_list[i]
		_df_result_filename = get_df_result_filename(_lambda)
		_root_dir = get_csv_root_dir(_lambda)
		
		df_result = calc_dim_rpca_ts(df, width, threshold, _lambda, maxIteration, theta, _root_dir)
		df_result.to_csv(_df_result_filename)
	
