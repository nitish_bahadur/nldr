﻿import numpy as np
import os, sys, math
import pandas as pd
from dateutil import parser
import scipy.sparse

from sklearn.preprocessing import StandardScaler

from dimredu.denseSolvers import denseToSparse as denseToSparse
from dimredu.sRPCAviaADMMFast import sRPCA as sRPCA

from sklearn.metrics import classification_report, confusion_matrix, f1_score
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor, GradientBoostingClassifier

	
def get_svd_dimension(S, threshold=0.9):
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    cum_s_exp = np.cumsum(s_exp)        
    for i in range(0, cum_s_exp.shape[0]):
        if cum_s_exp[i] >= threshold:
            return i
    return cum_s_exp.shape[0]    

def get_eig_dimension(S, threshold=0.9):
    """Calculate the dimension based on cumulative eigenvalue value percentages"""
    return get_svd_dimension(np.square(S), 0.9)

def get_svd_dim_gt_theta(S, theta = 0.01):
    """Calculate the dimension based on cumulative singular value percentages"""
    tot = sum(S)
    s_exp = [(i / tot) for i in sorted(S, reverse=True)]  # remove sorted
    s_gt_theta = [i for i in s_exp if i >= theta]
    return len(s_gt_theta)

def get_eig_dim_gt_theta(S, theta = 0.01):
    return get_svd_dim_gt_theta(np.square(S), theta)

def get_full_path_2_save(root_dir, filename):
    full_path = os.path.join(root_dir, filename)    
    return full_path	


	
# def calc_dim_rpca_ts(df, width, threshold, _lambda, maxIteration, theta, root_dir, etf_ticker):
# 	"""Loop through the data frame and then call Robust PCA"""
# 	max_rows = df.shape[0] - width    

# 	dates = []
# 	lambdas = []
# 	lambdas_scaled = []
# 	filenames = []

# 	rpca_svd_dimensions = []
# 	rpca_eig_dimensions = []
# 	rpca_svd_gt_1pct = []
# 	rpca_eig_gt_1pct = []

# 	epsilon1 = 1e-5
# 	epsilon2 = 1e-4
	
# 	for i in range(0, max_rows):
# 		M = df[i:i+width].copy()
# 		orig_cols_len = len(M.columns)
# 		M.dropna(axis=1, inplace=True)
# 		new_cols_len = len(M.columns)
# 		dt = df.index[i+width]
# 		filename_prefix = parser.parse(dt).strftime('%Y%m%d')
		
# 		if filename_prefix == "20190930":
# 			if new_cols_len > 5:
# 				# call dense solver here and get the input matrices
# 				Err = np.ones(M.shape)*1e-5
# 				m, n, u, v, vecM, vecEpsilon = denseToSparse(M.values, Err)
# 				maxRank = np.min(M.shape)
				
# 				scaled_lambda = get_scaled_lambda(M.shape[0], M.shape[1], _lambda)

# 				print("{}.) Now processing date = {} with _lambdaScaler = {} for {}".format(i, dt, _lambda, etf_ticker))
				
# 				# Robust PCA
# 				U, E, VT, S, B = sRPCA(m, n, u, v, vecM, vecEpsilon, maxRank, lam=_lambda, 
# 									   mu=None, rho=None, epsilon1=epsilon1, epsilon2=epsilon2,
# 									   truncateK=0, SOff=False,
# 									   maxIteration=maxIteration, verbose=False)
					  
# 				L = U * np.diag(E) * VT
# 				m = StandardScaler().fit_transform(L)
# 				_, Sigma, _ = np.linalg.svd(m)

# 				svd_dim = get_svd_dimension(Sigma, threshold)
# 				eig_dim = get_eig_dimension(Sigma, threshold)
# 				svd_gt1pct_dim = get_svd_dim_gt_theta(Sigma, theta)
# 				eig_gt1pct_dim = get_eig_dim_gt_theta(Sigma, theta)
			
# 				dates.append(dt)
# 				lambdas.append(_lambda)
# 				lambdas_scaled.append(scaled_lambda)
# 				filenames.append(filename_prefix)

				
# 				rpca_svd_dimensions.append(svd_dim)        
# 				rpca_eig_dimensions.append(eig_dim)
# 				rpca_svd_gt_1pct.append(svd_gt1pct_dim)
# 				rpca_eig_gt_1pct.append(eig_gt1pct_dim)

# 				# Store the matrix is a directory
# 				# Write the suffix to the top a file
				
# 				# Data is always written in ‘C’ order, independent of the order of matrix. 
# 				# The data produced by this method can be recovered using the function fromfile().
# 				U.dump(get_full_path_2_save(root_dir, '{}_{}_U.dat'.format(filename_prefix, _lambda)))
# 				E.dump(get_full_path_2_save(root_dir, '{}_{}_E.dat'.format(filename_prefix, _lambda)))
# 				VT.dump(get_full_path_2_save(root_dir, '{}_{}_VT.dat'.format(filename_prefix, _lambda)))
# 				L.dump(get_full_path_2_save(root_dir, '{}_{}_L.dat'.format(filename_prefix, _lambda)))
# 				scipy.sparse.save_npz(get_full_path_2_save(root_dir, '{}_{}_S.npz'.format(filename_prefix, _lambda)), S) # compressed matrix
# 				scipy.sparse.save_npz(get_full_path_2_save(root_dir, '{}_{}_B.npz'.format(filename_prefix, _lambda)), B) # compressed matrix

# 				# save the tickers
# 				myList = M.columns.tolist()
# 				myString = ",".join(myList )
# 				np.savetxt(get_full_path_2_save(root_dir, '{}_{}_tickers.csv'.format(filename_prefix, _lambda)), ["%s" % myString], fmt='%s')
# 		else:
# 			print("skipping: {}".format(filename_prefix))

# 	df1 = pd.DataFrame()
# 	df1['Dates'] = dates
# 	df1['LAMBDA'] = lambdas
# 	df1['LAMBDA_SCALED'] = lambdas_scaled
# 	df1['FILENAME_PREFIX'] = filenames
# 	df1['RPCA_SVD_DIMS'] = rpca_svd_dimensions
# 	df1['RPCA_EIG_DIMS'] = rpca_eig_dimensions
# 	df1['RPCA_SVD_GT_1PCT_DIMS'] = rpca_svd_gt_1pct
# 	df1['RPCA_EIG_GT_1PCT_DIMS'] = rpca_eig_gt_1pct

# 	return df1
	

X_TRAIN_FILE_PATH = r'../input/nbi_x_train.csv'
X_TEST_FILE_PATH = r'../input/nbi_x_test.csv'
X_VALIDATE_FILE_PATH = r'../input/nbi_x_validate.csv'

Y_TRAIN_FILE_PATH = r'../input/nbi_y_train.csv'
Y_TEST_FILE_PATH = r'../input/nbi_y_test.csv'
Y_VALIDATE_FILE_PATH = r'../input/nbi_y_validate.csv'

def get_nbi_input():
    """
    Load the data and calculate log return.  Remove the first row because it will always be NaN.

    Input file is created by first downloading adjusted closing prices of all 213 Nasdaq Biotechnology
    index constituents.  We are using ^NBI from www.nasdaq.com.

    Each matrix element is using excess returns, which is ticker daily return - index daily return

    We use 2016 and 2017 data in x_train, 2018 in test and 2019 in validate

    If an index return is > 2.5%, it is considered +1 (positive anomaly).  A return less than 
    -2.5% is considered -1 (negative anomaly).

    """
    x_train = load_df(X_TRAIN_FILE_PATH)
    y_train = load_df(Y_TRAIN_FILE_PATH)

    x_test = load_df(X_TEST_FILE_PATH)
    y_test = load_df(Y_TEST_FILE_PATH)

    x_validate = load_df(X_VALIDATE_FILE_PATH)
    y_validate = load_df(Y_VALIDATE_FILE_PATH)

    return x_train, x_test, x_validate, y_train, y_test, y_validate


def load_df(filepath):
    df = pd.read_csv(filepath) 
    df.set_index(df['Date'], inplace=True)
    df.drop('Date', axis=1, inplace=True)
    return df.values

def get_scaled_lambda(m, n, _lambda):
    lam = 1. / np.sqrt(np.max([m, n]))
    return lam, lam * _lambda

# function to run PCA and RPCA
def runAnalysis(X, lamScale):
    maxRank = np.linalg.matrix_rank(X)
    print("Max Rank: %d" % maxRank)
    T = np.asmatrix(X)  # gets shape of X
    u, v, vecM, vecEpsilon = [], [], [], []

    for i in range(T.shape[0]):
        for j in range(T.shape[1]):
            u.append(i)
            v.append(j)
            vecEpsilon.append(1e-5)     # NOTE original value is 1e-5
            Mij = float(T[i,j])
            vecM.append(Mij)

    u = np.array(u)
    v = np.array(v)
    vecM = np.array(vecM)
    vecEpsilon = np.array(vecEpsilon)

    def_lambda, scaleWant = get_scaled_lambda(T.shape[0], T.shape[1], lamScale)
    print("def_lambda: {} \t Multiplier: {} \t Lambda Scaled: {}".format(def_lambda, lamScale, scaleWant))

    # B is not used, but needs to be stored in order to run sRPCA
    [U, E, VT, S, B] = sRPCA(T.shape[0], T.shape[1], u, v, vecM, vecEpsilon, maxRank, lam=scaleWant)

    # Data is always written in ‘C’ order, independent of the order of matrix. 
    # The data produced by this method can be recovered using the function fromfile().
    output_dir = get_output_dir(lamScale)
    U.dump(get_full_path_2_save(output_dir, 'U_{}.dat'.format(lamScale)))
    E.dump(get_full_path_2_save(output_dir, 'E_{}.dat'.format(lamScale)))
    VT.dump(get_full_path_2_save(output_dir, 'VT_{}.dat'.format(lamScale)))
    # S.dump(get_full_path_2_save(output_dir, 'S_{}.dat'.format(lamScale)))
    scipy.sparse.save_npz(get_full_path_2_save(output_dir, 'S_{}.npz'.format(lamScale)), S) # compressed matrix
    scipy.sparse.save_npz(get_full_path_2_save(output_dir, 'B_{}.npz'.format(lamScale)), B) # compressed matrix

    S = S.todense()
    E = np.diag(E)
    ue = np.dot(U, E)
    L = np.dot(ue, VT)

    # print("OG SHAPES, X: %s  U: %s  E: %s  VT: %s  L: %s" % (str(X.shape), str(U.shape), str(E.shape), str(VT.shape), str(L.shape)))

    # L^hat = u^hat dot E^hat dot VT^hat
    hatRows = len(E[E > 0])
    Uhat = U[:,:hatRows]
    Ehat = np.diag(E[E > 0])
    VThat = VT[:hatRows]
    VTta = VT[hatRows:]

    # print("HAT SHAPES, Uhat: %s  Ehat: %s  VThat: %s  VTta: %s" % (str(Uhat.shape), str(Ehat.shape), str(VThat.shape), str(VTta.shape)))
    
    S.dump(get_full_path_2_save(output_dir, 'S_dense_{}.dat'.format(lamScale)))
    E.dump(get_full_path_2_save(output_dir, 'E_diag_{}.dat'.format(lamScale)))
    ue.dump(get_full_path_2_save(output_dir, 'ue_{}.dat'.format(lamScale)))    
    L.dump(get_full_path_2_save(output_dir, 'L_{}.dat'.format(lamScale)))
    VThat.dump(get_full_path_2_save(output_dir, 'VThat_{}.dat'.format(lamScale)))

    return S, L, VThat

def rf(X_train, X_test, y_train):#, maxDepth, nEst, randState):
    clf = RandomForestClassifier(random_state=0, n_jobs=-1, class_weight="balanced")
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    return y_pred

def check_output_dir(lambda_):
	output_dir = get_output_dir(lambda_)
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)
	return output_dir

def get_output_dir(lambda_):
	output_dir = r'../output/lambda_{}'.format(lambda_)
	return output_dir

# float range function
def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

if __name__ == "__main__":
	x_train, x_test, x_validate, y_train, y_test, y_validate = get_nbi_input()	
	# print('x_train shape: {}'.format(x_train.shape))
	# print('y_train shape: {}'.format(y_train.shape))
	# print('x_test shape: {}'.format(x_test.shape))
	# print('y_test shape: {}'.format(y_test.shape))
	# print('x_validate shape: {}'.format(x_validate.shape))
	# print('y_validate shape: {}'.format(y_validate.shape))

	# lambdas = [0.01, 0.015, 0.02, 0.025, 0.03, 0.035, 0.04, 0.045, 0.05, 0.055, 0.06, 0.065, 0.07, 0.075, 0.08, 0.085, 0.09, 0.095, 0.1]
	# lambdas = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1]
	lambdas = [0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.75, 0.8, 0.85, 0.9, 0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.25, 1.5, 2, 4, 8, 16, 32, 64, 100]
	# lambdas = [1, 5, 10, 15, 20, 25, 50, 100]
	# lambdas = [1, 10, 100, 1000]

	for lamScale in lambdas:
		output_dir = check_output_dir(lamScale)
		S1, L1, VThat = runAnalysis(X=x_train, lamScale=lamScale)
		print("x_train shapes: X: %s  L: %s  S: %s" % (str(x_train.shape), str(L1.shape), str(S1.shape)))		
		S1.dump(get_full_path_2_save(output_dir, 'S1_{}.dat'.format(lamScale)))
		L1.dump(get_full_path_2_save(output_dir, 'L1_{}.dat'.format(lamScale)))


		X2VTT = np.dot(x_test, VThat.T)
		L2 = np.dot(X2VTT, VThat)
		S2 = x_test - L2
		print("x_test shapes: X: %s  L: %s  S: %s" % (str(x_test.shape), str(L2.shape), str(S2.shape)))
		S2.dump(get_full_path_2_save(output_dir, 'S2_{}.dat'.format(lamScale)))
		L2.dump(get_full_path_2_save(output_dir, 'L2_{}.dat'.format(lamScale)))
		X2VTT.dump(get_full_path_2_save(output_dir, 'X2VTT_{}.dat'.format(lamScale)))


		# 2 step process, just run it for x_train and x_test
		print("Running it for X")
		y_pred = rf(x_train, x_test, y_train.ravel())
		# cfm = pd.crosstab(y_test.ravel(), y_pred, rownames=['Actual'], colnames=['Predicted'])
		print("X classification_report:")
		print("==========================================================================")
		print(classification_report(y_test, y_pred))
		print("==========================================================================")

		# test L + S
		print("Running it for L + S")
		LStrain = np.concatenate((L1, S1), axis=1)
		LStest = np.concatenate((L2, S2), axis=1)
		y_pred = rf(LStrain, LStest, y_train.ravel())
		print("L+S classification_report:")
		print("==========================================================================")
		print(classification_report(y_test, y_pred))
		print("==========================================================================")

		# test X + L + S
		print("Running it for X + L + S")
		XLStrain = np.concatenate((x_train, L1, S1), axis=1)
		XLStest = np.concatenate((x_test, L2, S2), axis=1)
		y_pred = rf(XLStrain, XLStest, y_train.ravel())
		print("X + L + S classification_report:")
		print("==========================================================================")
		print(classification_report(y_test, y_pred))
		print("==========================================================================")

		# test S
		print("Running it ONLY FOR S")
		y_pred = rf(S1, S2, y_train.ravel())
		print("S classification_report:")
		print("==========================================================================")
		print(classification_report(y_test, y_pred))
		print("==========================================================================")

	# then we run it for validation, don't run it for every iteration
	# that will allow us to snoop
	# for m in toRun:
	# 	res, dall = runModels(Xmat, Lmat, Smat, ymatX12, code=m)

	# 	if res:
	# 		print("Validating GOOD Lambda: %s" % (str(l)))

	# 		# validate
	# 		X3VTT = np.dot(x_validate, VThat.T)
	# 		L3 = np.dot(X3VTT, VThat)
	# 		S3 = x_validate - L3
	# 		print("x_validate shapes: X: %s  L: %s  S: %s" % (str(x_validate.shape), str(L3.shape), str(S3.shape)))

	# 		# ML/AI
	# 		Xmat, Lmat, Smat, ymatX13 = [x_train, x_validate], [L1, L3], [S1, S3], [y_train, y_test]

	# 		LStrain = np.concatenate((L[0],S[0]), axis=1)
	# 		XLStrain = np.concatenate((X[0], L[0], S[0]), axis=1)
	# 		LStest = np.concatenate((L[1],S[1]), axis=1)
	# 		XLStest = np.concatenate((X[1], L[1], S[1]), axis=1)
	# 		train = [X[0], LStrain, XLStrain]     # holds data matricies to run models on
	# 		test = [X[1], LStest, XLStest]


	# 		res, dgood = runModels(Xmat, Lmat, Smat, ymatX13, code=m)
	# 		goodData.append(dgood)  # XXX used for plotting
