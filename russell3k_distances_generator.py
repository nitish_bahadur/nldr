import numpy as np
import pandas as pd
import os
from sklearn import manifold
from numpy.linalg import svd
from datetime import datetime
from scipy.spatial.distance import pdist, squareform


INPUT_RETURNS_DIR = r'C:\phd\data\RUSSELL3K\returns'
OUTPUT_DIR = r'C:\phd\data\RUSSELL3K\distances'

def get_df(filename):
    df = pd.read_csv(filename)
    df.set_index(df['Dates'], inplace=True)
    df.drop('Dates', axis=1, inplace=True)    
    return df

def create_euclidean_dist_file(data, filename):
    distances = pdist(data, metric='euclidean')
    dist_matrix = squareform(distances)
    filepath = os.path.join(OUTPUT_DIR, filename + "_D.npy")
    np.save(filepath, dist_matrix)
    return dist_matrix

# def generate_daily_H_matrix(pr_df, width):
# 	max_rows = pr_df.shape[0] - (width + 1)
# 	for i in range(0, max_rows):
# 		data = pr_df[i:i+width].copy()
# 		orig_cols_len = len(data.columns)
# 		data.dropna(axis=1, inplace=True)
# 		new_cols_len = len(data.columns)
# 		ds = pr_df.index[i+width]

# 		filename = get_input_date_filename(ds)
# 		year = int(filename[0:4])

# 		if year in [2012, 2013, 2014, 2015, 2016]:
# 			s = '%d.) Now processing date = %s' % (i, ds)
# 			print(s)

# 			H = get_H(data, np.log(data))

# 			filepath = os.path.join(OUTPUT_DIR, filename + "_H.npy")
# 			np.save(filepath, H)

# def check_input_files():
# 	files = os.listdir(INPUT_RETURNS_DIR)
# 	for f in files:
# 		filepath = os.path.join(INPUT_RETURNS_DIR, f)
# 		df_ = pd.read_csv(filepath)
# 		data = df_.values
# 		nrows = data.shape[0]
# 		input_dim = data.shape[1]

# 		model_type = None
# 		if input_dim < 768:
# 			model_type = "mt_256"
# 		elif (input_dim >= 768) & (input_dim < 1536):
# 			model_type = "mt_512"
# 		else:
# 			model_type = "mt_1024"
# 		print("Name:{} Dimension:{}x{} Model:{}".format(f, nrows, input_dim, model_type))


if __name__ == "__main__":
	files = os.listdir(INPUT_RETURNS_DIR)
	for filename in files:
		filepath = os.path.join(INPUT_RETURNS_DIR, filename)
		year = int(filename[0:4])
		df = get_df(filepath)
		m = create_euclidean_dist_file(df.values, filename[0:8])
		print('Date: {} df size: {}x{} dist_matrix size: {}x{}'.format(filename[0:8], len(df), len(df.columns), m.shape[0], m.shape[1]))
		# break

	# pr_df = get_daily_pr(INPUT_PR_FILE_PATH)
	# generate_daily_H_matrix(pr_df, width=60)
	# generate_daily_returns_matrix(df, width=60)
	# print("loaded file: " + str(len(df)))
	# check_input_files()